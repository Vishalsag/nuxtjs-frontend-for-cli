# Inventory

> This is Inventory Management

## Prerequisite
+ Node
+ Git
+ Nuxtjs

``` bash
# install node
npm install node

# install git
pacman -S git

#install nuxt
npm install --save nuxt

```


## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

## Functionalities

+ Login
+ Profit
+ In Stock
+ Popular Item
+ Profitable Item
+ Daily Profit
+ Create
+ Read
+ Update
+ Delete

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).